Rails.application.routes.draw do
  resources :vacations, except: [:new, :edit]

  post '/in' => 'incoming_slack_commands#incoming'
  get '/ical/:user_id/cal.ics' => 'vacations#ical_person'
  get '/ical.ics' => 'vacations#ical'
end
