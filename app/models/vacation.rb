class Vacation < ActiveRecord::Base
  validates :start, :end, :user_id, :user_name, presence: true

  def to_ics
    event = Icalendar::Event.new
    event.dtstart = Icalendar::Values::Date.new(self.start.strftime("%Y%m%d"))
    event.dtend = Icalendar::Values::Date.new(self.end.strftime("%Y%m%d"))
    event.summary = calendar_title
    event.description = calendar_description
    event.created = self.created_at
    event.last_modified = self.updated_at
    event.uid = event.url = "localhost:3000/vacations/#{self.id}"
    event
  end

  private

  # This is the text as appears in your calendar overview
  def calendar_title
    "#{self.user_name} off"
  end

  # What appears when you click on a calendar item
  def calendar_description
    "#{user_name} is off from #{pretty_date(self.start)} to #{pretty_date(self.end)}\n\nReason: #{self.reason}\n\nTo remove this entry, use:\n /timeoff remove #{self.id}"
  end

  def pretty_date(date)
    date.strftime("%b %e %Y")
  end
end
